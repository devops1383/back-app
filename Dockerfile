FROM openjdk:8 AS TEMP_BUILD_IMAGE
ENV APP_HOME=/root/dev-school-app/
WORKDIR $APP_HOME
COPY build.gradle settings.gradle gradlew gradlew.bat $APP_HOME
COPY gradle $APP_HOME/gradle
COPY conf $APP_HOME/conf
COPY src $APP_HOME/src
RUN ./gradlew build
FROM openjdk:8-jre
COPY --from=TEMP_BUILD_IMAGE /root/dev-school-app/build/libs/dev-school-app-1.0-SNAPSHOT.jar .
EXPOSE 8080
CMD ["java","-jar","dev-school-app-1.0-SNAPSHOT.jar"]
